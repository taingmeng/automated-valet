import readline from 'readline';
import { processFile } from './src/controller/ParkingController';

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

const cli = () => {
  const instruction =
    '\n' +
    '- Enter the file path.\n' +
    '- Hit "enter" to use the sample input.\n' +
    '- Enter "q" to exit.\n' +
    '\n' +
    `Input file path: `;

  rl.question(instruction, async (filePath) => {
    if (filePath === 'q') {
      rl.close();
      return;
    }
    await processFile(filePath);
    cli();
  });
};

cli();
