# Welcome to Automated Valet 🅿️ 🚘

![Version](https://img.shields.io/badge/version-1.0.0-blue.svg?cacheSeconds=2592000)
![Version](https://img.shields.io/badge/npm-%3E%3D%205.0.0-brightgreen.svg?cacheSeconds=2592000)
![Version](https://img.shields.io/badge/node-%3E%3D%208.0.0-brightgreen.svg?cacheSeconds=2592000)
![Version](https://img.shields.io/badge/yarn-%3E%3D%201.0.0-blue.svg?cacheSeconds=2592000)
[![License: ISC](https://img.shields.io/badge/License-ISC-yellow.svg)](#)
![ts](https://badgen.net/badge/-/TypeScript?icon=typescript&label&labelColor=blue&color=555555)

> Automated Valet Car Parking Backend Exercise

## Features

- CLI prompting the user to input the file path.
- If no file path, default sample file is used.
- Enter "q" to exit the CLI.

```sh
- Enter the file path.
- Hit "enter" to use the sample input.
- Enter "q" to exit.

Input file path: assets/sample.txt
```

## Assumptions

- The input file is in the correct format.
- Numbers are valid integers.
- Vehicle types are only `car` and `motorcycle`.
- All strings are treated case-sensitively.
- Contiguous line breaks and contiguous white spaces are handled.
- Leading and trailing white spaces and new lines are handled.
- These invalid cases are handled:
  - Invalid command other than `Enter` and `Exit`.
  - Duplicated license plate number when enter.
  - Unknown license plate number when exit.
  - Invalid exit time (null or less than enter time).

## Prerequisites

- npm >= 5.0.0
- node >= 8.0.0
- yarn >= 1.0.0

## Install

```sh
yarn install
```

## Usage

```sh
yarn start
```

## Development

```sh
yarn dev
```

- Hot reloading is supported by nodemon

## Run tests

```sh
yarn test
```

- Run test in watch mode

## Coverage

```sh
yarn coverage
```

## Author

👤 **Meng Taing <taingmeng@gmail.com>**

- Twitter: [@taingmeng](https://twitter.com/taingmeng)
- Github: [@taingmeng](https://github.com/taingmeng)
