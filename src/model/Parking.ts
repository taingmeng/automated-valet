import { capitalize } from '../util/stringUtil';
import { VehicleType } from './VehicleType';

const CAR_FEE_PER_HOUR = 2;
const MOTORCYCLE_FEE_PER_HOUR = 1;

export class Parking {
  vehicleType: VehicleType;
  licensePlate: string;
  inTime: number;
  outTime?: number;
  lotIndex?: number;

  constructor(vehicleType: VehicleType, licensePlate: string, inTime: number) {
    this.vehicleType = vehicleType;
    this.licensePlate = licensePlate;
    this.inTime = inTime;
  }

  getFee = (): number => {
    const hours = Math.ceil((this.outTime - this.inTime) / (60 * 60));
    if (this.vehicleType === VehicleType.CAR) {
      return hours * CAR_FEE_PER_HOUR;
    } else {
      return hours;
    }
  };

  getLotName = (): string => {
    return `${capitalize(this.vehicleType)}Lot${this.lotIndex + MOTORCYCLE_FEE_PER_HOUR}`;
  };
}
