import { Parking } from './Parking';
import { VehicleType } from './VehicleType';

export default class ParkingValet {
  maxCapacities: {
    [key in VehicleType]: number;
  };
  lots: {
    [key in VehicleType]: (Parking | null)[];
  };
  licensePlateToVehicleTypeLookup: {
    [key in string]: VehicleType;
  };

  constructor(carLots: number, motorcycleLots: number) {
    this.maxCapacities = {
      [VehicleType.CAR]: carLots,
      [VehicleType.MOTORCYCLE]: motorcycleLots,
    };
    this.lots = {
      [VehicleType.CAR]: new Array(carLots).fill(null),
      [VehicleType.MOTORCYCLE]: new Array(motorcycleLots).fill(null),
    };
    this.licensePlateToVehicleTypeLookup = {};
  }

  enter = (parking: Parking): string => {
    const freeLotCount = this.lots[parking.vehicleType].filter((item) => !item)
      .length;
    if (freeLotCount === 0) {
      return 'Reject';
    }
    const hasParked = this.lots[parking.vehicleType].some(
      (lot) => lot && lot.licensePlate === parking.licensePlate,
    );
    if (hasParked) {
      throw new Error('Duplicated license plate.');
    }

    this.licensePlateToVehicleTypeLookup[parking.licensePlate] =
      parking.vehicleType;

    const freeIndex = this.lots[parking.vehicleType].findIndex(
      (lot) => lot === null,
    );

    parking.lotIndex = freeIndex;
    this.lots[parking.vehicleType][freeIndex] = parking;
    return `Accept ${parking.getLotName()}`;
  };

  exit = (licensePlate: string, outTime: number): string => {
    const vehicleType = this.licensePlateToVehicleTypeLookup[licensePlate];
    if (!vehicleType) {
      throw new Error('Unable to find the vehicle.');
    }

    const index = this.lots[vehicleType].findIndex(
      (lot) => lot && lot.licensePlate === licensePlate,
    );
    if (index === -1) {
      throw new Error('Unable to find the vehicle.');
    }

    const parking = this.lots[vehicleType][index];

    if (!outTime || outTime <= parking.inTime) {
      throw new Error('Invalid out time.');
    }
    parking.outTime = outTime;
    this.lots[vehicleType][index] = null;
    delete this.licensePlateToVehicleTypeLookup[licensePlate];
    return `${parking.getLotName()} ${parking.getFee()}`;
  };
}
