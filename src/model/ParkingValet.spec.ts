import { Parking } from './Parking';
import ParkingValet from './ParkingValet';
import { VehicleType } from './VehicleType';

const IN_TIME = 1613541902;

describe('ParkingValet', () => {
  describe('constructor', () => {
    it('should initialize valet with capacities based on the vehicle type', () => {
      const valet = new ParkingValet(10, 5);

      expect(valet.maxCapacities[VehicleType.CAR]).toEqual(10);
      expect(valet.maxCapacities[VehicleType.MOTORCYCLE]).toEqual(5);
    });

    it('should initialize valet with arrays filled null based on the vehicle type', () => {
      const valet = new ParkingValet(2, 3);

      expect(valet.lots[VehicleType.CAR]).toEqual([null, null]);
      expect(valet.lots[VehicleType.MOTORCYCLE]).toEqual([null, null, null]);
    });
  });

  describe('enter', () => {
    it('should park the car if there is available lot; otherwise reject', () => {
      const valet = new ParkingValet(2, 3);
      const parking1 = new Parking(VehicleType.CAR, 'test-car-1', IN_TIME);
      const parking2 = new Parking(VehicleType.CAR, 'test-car-2', IN_TIME);
      const parking3 = new Parking(VehicleType.CAR, 'test-car-3', IN_TIME);

      expect(valet.enter(parking1)).toEqual('Accept CarLot1');
      expect(valet.enter(parking2)).toEqual('Accept CarLot2');
      expect(valet.enter(parking3)).toEqual('Reject');
    });

    it('should park the motorcycle if there is available lot; otherwise reject', () => {
      const valet = new ParkingValet(3, 2);
      const parking1 = new Parking(
        VehicleType.MOTORCYCLE,
        'test-motorcycle-1',
        IN_TIME,
      );
      const parking2 = new Parking(
        VehicleType.MOTORCYCLE,
        'test-motorcycle-2',
        IN_TIME,
      );
      const parking3 = new Parking(
        VehicleType.MOTORCYCLE,
        'test-motorcycle-3',
        IN_TIME,
      );

      expect(valet.enter(parking1)).toEqual('Accept MotorcycleLot1');
      expect(valet.enter(parking2)).toEqual('Accept MotorcycleLot2');
      expect(valet.enter(parking3)).toEqual('Reject');
    });

    it('should handle duplicated license plate', () => {
      const valet = new ParkingValet(2, 3);
      const parking1 = new Parking(VehicleType.CAR, 'test-car-1', IN_TIME);
      const parking2 = new Parking(VehicleType.CAR, 'test-car-1', IN_TIME + 60);
      
      expect(valet.enter(parking1)).toEqual('Accept CarLot1');
      expect(() => valet.enter(parking2)).toThrowError('Duplicated license plate.');
    });

    it('should allocate the lowest number of lot', () => {
      const valet = new ParkingValet(2, 3);
      const parking1 = new Parking(VehicleType.CAR, 'test-car-1', IN_TIME);
      const parking2 = new Parking(VehicleType.CAR, 'test-car-2', IN_TIME + 60);
      valet.enter(parking1);
      valet.enter(parking2);
      valet.exit('test-car-2', IN_TIME + 120);

      const parking3 = new Parking(VehicleType.CAR, 'test-car-3', IN_TIME + 180);
      
      expect(valet.enter(parking3)).toEqual('Accept CarLot2');
    });
  });

  describe('exit', () => {
    it('should throw error if outTime <= inTime or outTime is provided', () => {
      const valet = new ParkingValet(2, 3);
      const parking = new Parking(VehicleType.CAR, 'test-car-1', IN_TIME);

      valet.enter(parking);

      expect(() => valet.exit('test-car-1', null)).toThrowError(
        'Invalid out time.',
      );
      expect(() => valet.exit('test-car-1', IN_TIME)).toThrowError(
        'Invalid out time.',
      );
      expect(() => valet.exit('test-car-1', IN_TIME - 60)).toThrowError(
        'Invalid out time.',
      );
    });

    it('should throw error if license plate is not found', () => {
      const valet = new ParkingValet(2, 3);
      const parking = new Parking(
        VehicleType.MOTORCYCLE,
        'test-motorcycle-1',
        IN_TIME,
      );

      valet.enter(parking);

      expect(() => valet.exit('test-unknown', IN_TIME + 3600)).toThrowError(
        'Unable to find the vehicle.',
      );
    });

    it('should throw error if the vehicle is somehow removed from the lot', () => {
      const valet = new ParkingValet(2, 3);
      const parking = new Parking(
        VehicleType.MOTORCYCLE,
        'test-motorcycle-1',
        IN_TIME,
      );
      valet.enter(parking);

      valet.lots[VehicleType.MOTORCYCLE][0] = null;

      expect(() =>
        valet.exit('test-motorcycle-1', IN_TIME + 3600),
      ).toThrowError('Unable to find the vehicle.');
    });

    it('should return lot name and fee for car and motorcycle', () => {
      const valet = new ParkingValet(3, 2);
      const parking1 = new Parking(VehicleType.CAR, 'test-car-1', IN_TIME);
      const parking2 = new Parking(
        VehicleType.MOTORCYCLE,
        'test-motorcycle-1',
        IN_TIME,
      );
      valet.enter(parking1);
      valet.enter(parking2);

      // rounded up 1.5 hours to 2 hours
      expect(valet.exit('test-car-1', IN_TIME + 5400)).toEqual('CarLot1 4');
      expect(valet.exit('test-motorcycle-1', IN_TIME + 5400)).toEqual(
        'MotorcycleLot1 2',
      );
    });
  });
});
