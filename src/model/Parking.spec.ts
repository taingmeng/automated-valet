import { Parking } from './Parking';
import { VehicleType } from './VehicleType';

const IN_TIME = 1613541902;

describe('Parking', () => {
  describe('getLotName', () => {
    it('should return CarLot1 for car at index 0', () => {
      const parking = new Parking(VehicleType.CAR, 'test_plate', IN_TIME);
      parking.lotIndex = 0;

      expect(parking.getLotName()).toEqual('CarLot1');
    });

    it('should return MotorcycleLot1 for motorcycle at index 0', () => {
      const parking = new Parking(
        VehicleType.MOTORCYCLE,
        'test_plate',
        IN_TIME,
      );
      parking.lotIndex = 0;

      expect(parking.getLotName()).toEqual('MotorcycleLot1');
    });
  });

  describe('getFee', () => {
    it('should return $2 when car is parked exactly 1 hour', () => {
      const parking = new Parking(VehicleType.CAR, 'test_plate', IN_TIME);
      parking.outTime = IN_TIME + 3600;

      expect(parking.getFee()).toEqual(2);
    });

    it('should return $4 when car is parked slightly more than 1 hour', () => {
      const parking = new Parking(VehicleType.CAR, 'test_plate', IN_TIME);
      parking.outTime = IN_TIME + 3700;

      expect(parking.getFee()).toEqual(4);
    });

    it('should return $1 when motorcycle is parked exactly 1 hour', () => {
      const parking = new Parking(
        VehicleType.MOTORCYCLE,
        'test_plate',
        IN_TIME,
      );
      parking.outTime = IN_TIME + 3600;

      expect(parking.getFee()).toEqual(1);
    });

    it('should return $2 when motorcycle is parked slightly more than 1 hour', () => {
      const parking = new Parking(
        VehicleType.MOTORCYCLE,
        'test_plate',
        IN_TIME,
      );
      parking.outTime = IN_TIME + 3700;

      expect(parking.getFee()).toEqual(2);
    });
  });
});
