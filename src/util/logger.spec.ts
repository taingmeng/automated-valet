import { logger } from './logger';

describe('Name of the group', () => {
  describe('log', () => {
    it('should call console.log', () => {
      jest.spyOn(console, 'log').mockImplementationOnce(null);
      logger.log('test', { foo: 'bar' });

      expect(console.log).toHaveBeenCalledWith('test', { foo: 'bar' });
    });
  });

  describe('error', () => {
    it('should call console.error', () => {
      jest.spyOn(console, 'error').mockImplementationOnce(null);
      logger.error('test', { foo: 'bar' });

      expect(console.error).toHaveBeenCalledWith('error:', 'test', {
        foo: 'bar',
      });
    });
  });
});
