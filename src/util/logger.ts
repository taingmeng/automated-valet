export const logger = {
  log: (...args): void => console.log(...args),
  error: (...args): void => console.error('error:', ...args),
};
