import * as fs from 'fs';
import { logger } from '../util/logger';
import { Parking } from '../model/Parking';
import ParkingValet from '../model/ParkingValet';
import { VehicleType } from '../model/VehicleType';

const processCommand = (line: string, valet: ParkingValet) => {
  try {
    if (line.startsWith('Enter')) {
      const [, vehicleType, licensePlate, inTime] = line.split(/\s+/);
      const parking = new Parking(
        vehicleType as VehicleType,
        licensePlate,
        parseInt(inTime, 10),
      );
      logger.log(valet.enter(parking));
    } else if (line.startsWith('Exit')) {
      const [, licensePlate, outTime] = line.split(/\s+/);
      logger.log(valet.exit(licensePlate, parseInt(outTime, 10)));
    } else {
      throw new Error('Invalid command.');
    }
  } catch (error) {
    logger.log((error as Error).message);
  }
};

export const processFile = async (filePath: string): Promise<void> => {
  try {
    const input = await fs.readFileSync(
      filePath || 'assets/sample.txt',
      'utf8',
    );
    const lines = input.replace(/^\s+|\s+$/g, '').split(/\n+/);
    const [availableCarLots, availableMotorcycleLots] = lines[0]
      .split(' ')
      .map((i) => parseInt(i, 10));

    const valet = new ParkingValet(availableCarLots, availableMotorcycleLots);

    lines.slice(1).forEach((line) => {
      const trimmed = line.trim();
      if (trimmed) {
        processCommand(trimmed, valet);
      }
    });
  } catch (error) {
    logger.error((error as Error).message);
  }
};
