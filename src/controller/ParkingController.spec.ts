import { logger } from '../util/logger';
import { processFile } from './ParkingController';

describe('ParkingController', () => {
  describe('processFile', () => {
    it('should process sample file if no file path', async () => {
      jest.spyOn(logger, 'log').mockImplementation(null);
      await processFile(null);

      expect((logger.log as jest.Mock).mock.calls).toEqual([
        ['Accept MotorcycleLot1'],
        ['Accept CarLot1'],
        ['MotorcycleLot1 2'],
        ['Accept CarLot2'],
        ['Accept CarLot3'],
        ['Reject'],
        ['CarLot3 6'],
      ]);
    });

    it('should log error if file is not found', async () => {
      jest.spyOn(logger, 'error').mockImplementation(null);
      await processFile('unknown-file.txt');

      expect(logger.error).toHaveBeenCalledWith(
        "ENOENT: no such file or directory, open 'unknown-file.txt'",
      );
    });

    it('should handle unknown license plate, invalid time, unknown command', async () => {
      jest.spyOn(logger, 'log').mockImplementation(null);
      await processFile('test/input/parking_invalid_cases.txt');

      expect((logger.log as jest.Mock).mock.calls).toEqual([
        ['Accept MotorcycleLot1'],
        ['Accept MotorcycleLot2'],
        ['Accept CarLot1'],
        ['Unable to find the vehicle.'],
        ['Invalid out time.'],
        ['Invalid command.'],
        ['Accept CarLot2'],
        ['Duplicated license plate.'],
      ]);
    });
  });
});
